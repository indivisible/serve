package wtf.indivisible.serve.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wtf.indivisible.serve.generic.Serve;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Created 15/01/15.
 */
public class Startup implements ServletContextListener {

    private static Logger mLog = LoggerFactory.getLogger(Startup.class);

    @Override
    public void contextInitialized(final ServletContextEvent event) {
        mLog.info("init: >>>   Starting serve-api   <<<");

        Serve.init();
    }

    @Override
    public void contextDestroyed(final ServletContextEvent event) {
        mLog.info("destroy: >>>   Stopping server-api   <<<");
    }
}

package wtf.indivisible.serve.api;

import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.oauth1.signature.OAuth1SignatureFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;

import javax.ws.rs.ApplicationPath;

/**
 * Created 17/01/15.
 */
@ApplicationPath("/")
public class ServeApi extends ResourceConfig {

    public ServeApi() {

        packages("wtf.indivisible.serve.api.endpoints");
        //packages("com.getsnipper.api.v3.providers");

        //register(ObjectMapperContextResolver.class);
        register(JacksonFeature.class);
        register(MultiPartFeature.class);
        register(OAuth1SignatureFeature.class);
        register(RolesAllowedDynamicFeature.class);

        setApplicationName("serve-api");

    }

}

package wtf.indivisible.serve.api.endpoints;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wtf.indivisible.serve.api.responses.Response;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;

/**
 * Created 29/12/14.
 */
@Path("/status")
public class StatusResource {

    @Context
    private SecurityContext mContext;
    @Context
    private HttpServletRequest mRequest;

    private static Logger mLog = LoggerFactory.getLogger(StatusResource.class);


    @GET
    public Response ping() {
        //TODO: Event class to help with logging
        mLog.info("Pinged by " + mRequest.getRemoteAddr());    //TODO: IP here
        return new Response(200, "OK");
    }


}

package wtf.indivisible.serve.api.endpoints;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wtf.indivisible.serve.api.responses.EnvironmentResponse;
import wtf.indivisible.serve.generic.Serve;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;

/**
 * Created 17/01/15.
 */
@Path("/env")
public class EnvironmentResource {

    @Context
    private ServletContext mContext;
    @Context
    private HttpServletRequest mRequest;

    private static Logger mLog = LoggerFactory.getLogger(EnvironmentResource.class);

    public EnvironmentResource() {
    }

    @GET
    public EnvironmentResponse getEnvironment() {
        mLog.trace("getEnvironment: requested from " + mRequest.getRemoteAddr());
        return new EnvironmentResponse(Serve.getEnvironment());
    }
}

package wtf.indivisible.serve.api.responses;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created 29/12/14.
 */
@XmlType(name = "response")
@XmlAccessorType(XmlAccessType.NONE)
public class Response {

    @XmlElement(name = "status")
    private int mHttpStatusCode;
    @XmlElement(name = "context")
    private String mErrorContext;
    @XmlElement(name = "type")
    private String mErrorSubType;
    @XmlElement(name = "message")
    private String mErrorMessage;

    public Response() {
        mHttpStatusCode = 200;
    }

    public Response(final int statusCode) {
        mHttpStatusCode = statusCode;
    }

    public Response(final int statusCode, final String message) {
        mHttpStatusCode = statusCode;
        mErrorMessage = message;
    }


    ////////////////////////////////////////////////////////////////////////////
    ////    methods
    ////////////////////////////////////////////////////////////////////////////

    public void error(final String errorContext, final String errorSubType, final String errorMessage) {
        mErrorContext = errorContext;
        mErrorSubType = errorSubType;
        mErrorMessage = errorMessage;
    }


    ////////////////////////////////////////////////////////////////////////////
    ////    gets & sets
    ////////////////////////////////////////////////////////////////////////////

    public int getHttpStatusCode() {
        return mHttpStatusCode;
    }

    public void setHttpStatusCode(final int httpStatusCode) {
        mHttpStatusCode = httpStatusCode;
    }

    public String getErrorContext() {
        return mErrorContext;
    }

    public void setErrorContext(final String errorContext) {
        mErrorContext = errorContext;
    }

    public String getErrorSubType() {
        return mErrorSubType;
    }

    public void setErrorSubType(final String errorSubType) {
        mErrorSubType = errorSubType;
    }

    public String getErrorMessage() {
        return mErrorMessage;
    }

    public void setErrorMessage(final String errorMessage) {
        mErrorMessage = errorMessage;
    }

}

package wtf.indivisible.serve.api.endpoints;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wtf.indivisible.serve.api.responses.Response;
import wtf.indivisible.serve.generic.Serve;
import wtf.indivisible.serve.generic.environment.Deployment;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import java.io.IOException;

/**
 * Created 17/01/15.
 */
@Path("/")
public class RootResource {

    @Context
    private HttpServletRequest request;

    @Context
    private HttpServletResponse response;

    @Context
    private ServletContext context;

    private static Logger mLog = LoggerFactory.getLogger(RootResource.class);


    @GET
    public Response root() {
        mLog.info("Request made to root.");

        Deployment deployment;
        if (Serve.getEnvironment() == null) {
            deployment = Deployment.BUILD;
        } else {
            deployment = Serve.getEnvironment().getDeployment();
        }

        switch (deployment) {

            case PRODUCTION:
            case ACCEPTANCE:
                try {
                    response.sendRedirect("http://indivisible.wtf");
                } catch (IOException e) {
                    mLog.error("Unable to redirect GET to Root");
                }
                break;
            case DEVELOPMENT:
                break;
            case TEST:
                break;
            case BUILD:
                break;
        }

        return new Response(200, "Hello, how may I Serve you?");
    }
}

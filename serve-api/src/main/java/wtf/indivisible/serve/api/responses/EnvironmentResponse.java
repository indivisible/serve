package wtf.indivisible.serve.api.responses;

import com.sun.istack.internal.NotNull;
import wtf.indivisible.serve.generic.environment.Environment;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created 17/01/15.
 */
@XmlAccessorType(XmlAccessType.NONE)
public class EnvironmentResponse extends Response {

    @XmlElement(name = "environment")
    private Environment mEnvironment;

    public EnvironmentResponse(@NotNull final Environment environment) {
        mEnvironment = environment;
    }

    public Environment getEnvironment() {
        return mEnvironment;
    }

    public void setEnvironment(final Environment environment) {
        mEnvironment = environment;
    }
}

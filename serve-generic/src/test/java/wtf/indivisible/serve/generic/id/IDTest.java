package wtf.indivisible.serve.generic.id;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static junit.framework.TestCase.assertTrue;

/**
 * Created 11/01/15.
 */
public class IDTest {

    private static Logger mLog = LoggerFactory.getLogger(IDTest.class);

    @Test
    public void generateAndValidateTest() {

        String id;
        for (int i = 0; i < 10; i++) {
            id = IDHelper.generate();

            assertTrue("Generated ID was null.", id != null);
            assertTrue("Generated ID's value returned null.", id.toString() != null);
            assertTrue("Generated ID's value was empty.", !id.toString().isEmpty());
            assertTrue("Generated ID failed validation: " + id.toString(), IDHelper.isValid(id));

            assertTrue("Generated ID did not equal itself.", id.equals(id));
        }
    }
}

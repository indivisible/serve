package wtf.indivisible.serve.generic.utils;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

/**
 * Created 17/01/15.
 */
public class LogicUtilTest {

    private static final String[] notNullOrEmptyStrings = {
            "abc",
            " b ",
            "   "
    };

    private static final String[] notNullOrEmptyWithTrimStrings = {
            "abc",
            " b "
    };

    private static final String[] isNullOrEmptyStrings = {
            null,
            ""
    };

    private static final String[] isNullOrEmptyWithTrimStrings = {
            null,
            "",
            "  "
    };


    @Test
    public void notNullOrEmptyTest() {
        for (String str : notNullOrEmptyStrings) {
            assertTrue("notNullOrEmptyTest: String failed to pass - " + str,
                    LogicUtil.notNullOrEmpty(str));
        }
    }

    @Test
    public void notNullOrEmptyWithTrimTest() {
        for (String str : notNullOrEmptyWithTrimStrings) {
            assertTrue("notNullOrEmptyWithTrimTest: String failed to pass - " + str,
                    LogicUtil.notNullOrEmptyWithTrim(str));
        }
    }

    @Test
    public void isNullOrEmptyTest() {
        for (String str : isNullOrEmptyStrings) {
            assertTrue("isNullOrEmptyTest: String failed to pass - " + str, LogicUtil.isNullOrEmpty(str));
        }
    }

    @Test
    public void isNullOrEmptyTestWithTrim() {
        for (String str : isNullOrEmptyWithTrimStrings) {
            assertTrue("isNullOrEmptyWithTrimTest: String failed to pass - " + str, LogicUtil.isNullOrEmptyWithTrim(str));
        }
    }
}

package wtf.indivisible.serve.generic.id;

import com.sun.istack.internal.NotNull;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 * Created 11/01/15.
 */
@XmlAccessorType(XmlAccessType.NONE)
public class ID implements wtf.indivisible.serve.interfaces.generic.id.ID {


    ////////////////////////////////////////////////////////////////////////////
    ////    data & init
    ////////////////////////////////////////////////////////////////////////////


    private String mUid;


    public ID(@NotNull final String uid) {
        mUid = uid;
    }


    ////////////////////////////////////////////////////////////////////////////
    ////    overrides
    ////////////////////////////////////////////////////////////////////////////

    @Override
    public String toString() {
        return mUid;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        wtf.indivisible.serve.generic.id.ID id = (wtf.indivisible.serve.generic.id.ID) o;

        if (!mUid.equals(id.mUid)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return mUid.hashCode();
    }

}

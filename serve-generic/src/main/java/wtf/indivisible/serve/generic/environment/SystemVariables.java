package wtf.indivisible.serve.generic.environment;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wtf.indivisible.serve.interfaces.generic.environment.PlatformType;

import java.io.File;
import java.util.Locale;

/**
 * Created 15/01/15.
 */
public class SystemVariables {

    ////////////////////////////////////////////////////////////////////////////
    ////    keys
    ////////////////////////////////////////////////////////////////////////////

    //------------------------------------------------------------------------//
    //  environment

    public static final String KEY_ENV_NAME = "serve.environment.name";
    public static final String KEY_ENV_TAG = "serve.environment.tag";
    public static final String KEY_ENV_LOCATION = "serve.environment.location";
    public static final String KEY_ENV_DEPLOYMENT = "serve.environment.deployment";
    public static final String KEY_ENV_LOG_LEVEL = "serve.environment.log-level";


    //------------------------------------------------------------------------//
    //  platform

    public static final String KEY_PLATFORM_TYPE = "serve.platform.type";
    public static final String KEY_PLATFORM_HOME = "serve.platform.home";

    private static final Logger mLog = LoggerFactory.getLogger(SystemVariables.class);


    ////////////////////////////////////////////////////////////////////////////
    ////    generic
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Get a String.
     * <p/>
     * Returns {@code null} if no key set. <br/>
     * Empty String is a valid value.
     *
     * @param key
     * @return
     */
    public static String getString(final String key) {

        try {
            String value = System.getProperty(key);
            if (value == null) {
                mLog.warn("getString: No value found for " + key);
            }
            return value;
        } catch (SecurityException e) {
            mLog.error("getString: Unable to read SystemProperties.");
            mLog.error("getString: " + e.getMessage());
            return null;
        }
    }

    /**
     * Get an Integer.
     * <p/>
     * Returns {@code null} if no key set or numeric value found.
     *
     * @param key
     * @return
     */
    public static Integer getInteger(final String key) {

        String strValue = null;
        try {
            strValue = getString(key);
            if (strValue != null) {
                return Integer.parseInt(strValue.trim());
            }
        } catch (NumberFormatException e) {
            mLog.error("getInteger: failed to parse value for integer - [" + strValue + "]");
        }
        return null;
    }

    ////////////////////////////////////////////////////////////////////////////
    ////    specific
    ////////////////////////////////////////////////////////////////////////////

    //------------------------------------------------------------------------//
    //  environment

    public static String getEnvironmentName() {
        return getString(KEY_ENV_NAME);
    }

    public static String getEnvironmentTag() {
        return getString(KEY_ENV_TAG);
    }

    public static Deployment getEnvironmentDeployment() {
        String value = getString(KEY_ENV_DEPLOYMENT);
        if (value == null) {
            mLog.error("getEnvironmentDeployment: No deployment set. Defaulting to BUILD");
            return Deployment.BUILD;
        }

        value = value.toLowerCase(Locale.ENGLISH).trim();
        if (value.startsWith("prod")) {
            return Deployment.PRODUCTION;
        } else if (value.startsWith("acc")) {
            return Deployment.ACCEPTANCE;
        } else if (value.startsWith("dev")) {
            return Deployment.DEVELOPMENT;
        } else if (value.startsWith("test")) {
            return Deployment.TEST;
        } else if (value.startsWith("build")) {
            return Deployment.BUILD;
        } else {
            mLog.warn("getEnvironmentDeployment: Unable to parse value set. Defaulting to BUILD");
            return Deployment.BUILD;
        }
    }

    public static String getLocation() {
        return getString(KEY_ENV_LOCATION);
    }

    //------------------------------------------------------------------------//
    //  platform

    public File getHome() {
        String value = getString(KEY_PLATFORM_HOME);
        if (value == null) {
            return null;
        }

        File file = new File(value);

        if (!file.exists()) {
            mLog.error("getHome: Home directory not exists - [{}]", value);
            return null;
        } else if (!file.isDirectory()) {
            mLog.error("getHome: Home directory not a directory - [{}]", value);
            return null;
        } else if (!file.canRead()) {
            mLog.error("getHome: Cannot read Home directory - [{}]", value);
            return null;
        } else if (!file.canWrite()) {
            mLog.warn("getHome: Cannot write to Home directory = [{}]", value);
        }
        return file;
    }

    public static PlatformType getPlatformType() {
        String value = getString(KEY_PLATFORM_TYPE);
        if (value == null) {
            return null;
        }
        return PlatformType.fromValue(value);
    }


}

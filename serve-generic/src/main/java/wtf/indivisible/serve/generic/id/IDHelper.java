package wtf.indivisible.serve.generic.id;

import java.util.Locale;
import java.util.UUID;
import java.util.regex.Pattern;

/**
 * Created 11/01/15.
 */
public class IDHelper implements wtf.indivisible.serve.interfaces.generic.id.IDHelper {

    private static final String REGEX_UUID = "[A-F0-9]{8}-[A-F0-9]{4}-[A-F0-9]{4}-[A-F0-9]{4}-[A-F0-9]{12}";
    private static final Pattern PATTERN_UUID = Pattern.compile(REGEX_UUID);

    public static String generate() {
        return UUID.randomUUID().toString().toUpperCase(Locale.ENGLISH);
    }

    public static boolean isValid(final String id) {
        if (id == null) {
            return false;
        }
        return PATTERN_UUID.matcher(id).matches();
    }
}

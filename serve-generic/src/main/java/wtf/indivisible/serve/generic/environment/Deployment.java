package wtf.indivisible.serve.generic.environment;

/**
 * Created 13/01/15.
 */
public enum Deployment {

    PRODUCTION,
    ACCEPTANCE,
    DEVELOPMENT,
    TEST,
    BUILD;

}

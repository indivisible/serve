package wtf.indivisible.serve.generic.environment;

import com.sun.istack.internal.NotNull;
import wtf.indivisible.serve.interfaces.generic.environment.PlatformType;
import wtf.indivisible.serve.interfaces.generic.exception.InitException;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created 11/01/15.
 */

@XmlRootElement(name = "platform")
@XmlAccessorType(XmlAccessType.NONE)
public class Platform implements wtf.indivisible.serve.interfaces.generic.environment.Platform {

    ////////////////////////////////////////////////////////////////////////////
    ////    data & init
    ////////////////////////////////////////////////////////////////////////////

    @XmlElement(name = "id")
    private String mID;
    @XmlElement(name = "name")
    private String mName;
    @XmlElement(name = "type")
    private PlatformType mPlatformType;
    @XmlElement(name = "version")
    private String mVersion;

    public Platform(final String id,
                    final String name,
                    final PlatformType platformType,
                    final String version)
            throws InitException {

        if (id == null || name == null || platformType == null || version == null) {
            throw new InitException("Unable to read all values for Platform.");
        }

        mID = id;
        mName = name;
        mPlatformType = platformType;
        mVersion = version;
    }


    ////////////////////////////////////////////////////////////////////////////
    ////    gets & sets
    ////////////////////////////////////////////////////////////////////////////


    public String getID() {
        return mID;
    }

    public void setID(final String id) {
        mID = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(final String name) {
        mName = name;
    }

    public PlatformType getPlatformType() {
        return mPlatformType;
    }

    public void setPlatformType(final PlatformType platformType) {
        mPlatformType = platformType;
    }

    @Override
    public PlatformType getType() {
        return mPlatformType;
    }

    @Override
    public void setType(@NotNull final PlatformType type) {
        mPlatformType = type;
    }

    @Override
    public String getVersion() {
        return mVersion;
    }

    @Override
    public void setVersion(@NotNull final String version) {
        mVersion = version;
    }
}

package wtf.indivisible.serve.generic.environment;

import com.sun.istack.internal.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wtf.indivisible.serve.interfaces.generic.environment.Platform;
import wtf.indivisible.serve.interfaces.generic.exception.InitException;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created 11/01/15.
 */
@XmlRootElement(name = "environment")
@XmlAccessorType(XmlAccessType.NONE)
public class Environment implements wtf.indivisible.serve.interfaces.generic.environment.Environment {


    ////////////////////////////////////////////////////////////////////////////
    ////    data & init
    ////////////////////////////////////////////////////////////////////////////

    @XmlElement(name = "id")
    private String mId;
    @XmlElement(name = "name")
    private String mName;
    @XmlElement(name = "tag")
    private String mTag;
    @XmlElement(name = "location")
    private String mLocation;

    @XmlElement(name = "platform")
    private Platform mPlatform;
    @XmlElement(name = "deployment")
    private Deployment mDeployment;

    private Logger mLog = LoggerFactory.getLogger(Environment.class);


    public Environment(@NotNull final String id,
                       @NotNull final String name,
                       @NotNull final String tag,
                       @NotNull final String location,
                       @NotNull final Platform platform,
                       @NotNull final Deployment deployment)
            throws InitException {

        if (id == null || name == null || tag == null
                || location == null || platform == null || deployment == null) {
            throw new InitException("Unable to read all values for Environment");
        }

        mId = id;
        mName = name;
        mTag = tag;
        mPlatform = platform;
        mDeployment = deployment;

        mLog.info("Environment initialised.");
    }


    ////////////////////////////////////////////////////////////////////////////
    ////    gets & sets
    ////////////////////////////////////////////////////////////////////////////

    public String getId() {
        return mId;
    }

    public void setId(@NotNull final String id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(@NotNull final String name) {
        mName = name;
    }

    public String getTag() {
        return mTag;
    }

    public void setTag(final String tag) {
        mTag = tag;
    }

    public String getLocation() {
        return mLocation;
    }

    public void setLocation(final String location) {
        mLocation = location;
    }

    public Platform getPlatform() {
        return mPlatform;
    }

    public void setPlatform(@NotNull final Platform platform) {
        mPlatform = platform;
    }

    public Deployment getDeployment() {
        return mDeployment;
    }

    public void setDeployment(final Deployment deployment) {
        mDeployment = deployment;
    }

}

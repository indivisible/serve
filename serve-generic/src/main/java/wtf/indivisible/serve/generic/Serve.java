package wtf.indivisible.serve.generic;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wtf.indivisible.serve.generic.environment.Deployment;
import wtf.indivisible.serve.generic.environment.Environment;
import wtf.indivisible.serve.generic.environment.Platform;
import wtf.indivisible.serve.generic.environment.SystemVariables;
import wtf.indivisible.serve.generic.id.IDHelper;
import wtf.indivisible.serve.interfaces.generic.environment.PlatformType;
import wtf.indivisible.serve.interfaces.generic.exception.InitException;

/**
 * Created 15/01/15.
 */
public class Serve {

    private static Environment mEnvironment;

    private static final String VERSION = "0.0.1";
    private static Logger mLog = LoggerFactory.getLogger(Serve.class);

    public static void init() {

        final String envID = IDHelper.generate();

        final String envName = SystemVariables.getEnvironmentName();
        final String envTag = SystemVariables.getEnvironmentTag();
        final Deployment envDeployment = SystemVariables.getEnvironmentDeployment();
        final String envLocation = SystemVariables.getLocation();
        final PlatformType platformType = SystemVariables.getPlatformType();

        try {
            final Platform platform = new Platform(envID, envName, platformType, VERSION);
            mEnvironment = new Environment(envID, envName, envTag, envLocation, platform, envDeployment);
        } catch (InitException e) {
            mLog.error("init: Failed to initialise Environment - {}", e.getMessage());
            //System.exit(10);
        }

        mLog.info("init: New Environment [{}]", mEnvironment.getId());
        mLog.debug("init: Name = {}", mEnvironment.getName());
        mLog.debug("init: Deployment = {}", mEnvironment.getDeployment().toString());
        mLog.debug("init: PlatformType = {}", mEnvironment.getPlatform().getPlatformType().toString());
    }

    public static Environment getEnvironment() {
        return mEnvironment;
    }

}

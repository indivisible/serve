package wtf.indivisible.serve.generic.utils;

/**
 * Created 17/01/15.
 */
public class LogicUtil {

    public static boolean notNullOrEmpty(final String str) {
        return str != null && !str.isEmpty();
    }

    public static boolean notNullOrEmptyWithTrim(final String str) {
        if (str == null) {
            return false;
        }
        return notNullOrEmpty(str.trim());
    }

    public static boolean isNullOrEmpty(final String str) {
        return str == null || str.isEmpty();
    }

    public static boolean isNullOrEmptyWithTrim(final String str) {
        if (str == null) {
            return isNullOrEmpty(str);
        }
        return isNullOrEmpty(str.trim());
    }
}

package wtf.indivisible.serve.generic.task;

/**
 * Created 17/01/15.
 */
public abstract class Task implements Runnable {

    @Override
    public void run() {

    }

    public abstract void work();
}

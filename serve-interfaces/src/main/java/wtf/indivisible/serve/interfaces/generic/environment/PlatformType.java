package wtf.indivisible.serve.interfaces.generic.environment;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.HashMap;
import java.util.Map;

/**
 * Created 11/01/15.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public enum PlatformType {

    UNKNOWN("unknown"),
    LINUX("linux"),
    OSX("osx"),
    WINDOWS("windows"),
    ANDROID("android"),
    IOS("ios"),
    PI("raspberrypi"),
    ARDUINO("arduino");


    ////////////////////////////////////////////////////////////////////////////
    ////    data & init
    ////////////////////////////////////////////////////////////////////////////

    @XmlElement(name = "platform")
    private String mValue;
    private static Map<String, PlatformType> mMap;

    private PlatformType(final String value) {
        mValue = value;
    }


    ////////////////////////////////////////////////////////////////////////////
    ////
    ////////////////////////////////////////////////////////////////////////////

    public String getValue() {
        return mValue;
    }

    public static PlatformType fromValue(final String value) {
        if (mMap == null) {
            mMap = initMap();
        }
        return mMap.get(value);
    }

    private static Map<String, PlatformType> initMap() {
        Map<String, PlatformType> map = new HashMap<String, PlatformType>();
        for (PlatformType type : PlatformType.values()) {
            map.put(type.getValue(), type);
        }
        return map;
    }

}

package wtf.indivisible.serve.interfaces.service.web;

import java.net.URL;

/**
 * Base Class for
 */
public interface Host {

    /**
     * Get the common name for this Host.
     *
     * @return
     */
    String getName();

    /**
     * Get the unique
     * @return
     */
    String getId();

    int getPort();

    URL getURL();
}

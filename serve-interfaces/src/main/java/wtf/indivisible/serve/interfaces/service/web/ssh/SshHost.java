package wtf.indivisible.serve.interfaces.service.web.ssh;

import wtf.indivisible.serve.interfaces.service.web.Host;

/**
 * Description and connection details for a SshHost.
 * <p/>
 * {@link wtf.indivisible.serve.interfaces.service.web.Host}
 * <p/>
 * Created 11/01/15.
 */
public interface SshHost extends Host {


}

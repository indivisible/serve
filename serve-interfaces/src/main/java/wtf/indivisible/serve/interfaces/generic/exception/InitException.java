package wtf.indivisible.serve.interfaces.generic.exception;

import com.sun.istack.internal.NotNull;

/**
 * Created 17/01/15.
 */
public class InitException extends RuntimeException {

    public InitException() {
        super();
    }

    public InitException(@NotNull final String message) {
        super(message);
    }

    public InitException(@NotNull final String message, final Throwable throwable) {
        super(message, throwable);
    }

    public InitException(final Throwable throwable) {
        super(throwable);
    }

}

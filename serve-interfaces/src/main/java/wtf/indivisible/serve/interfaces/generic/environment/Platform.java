package wtf.indivisible.serve.interfaces.generic.environment;

import com.sun.istack.internal.NotNull;

/**
 * Created 11/01/15.
 */
public interface Platform {

    String getID();

    void setID(final String ID);

    String getName();

    void setName(final String name);

    @NotNull
    PlatformType getPlatformType();

    void setPlatformType(@NotNull final PlatformType platformType);

    @NotNull
    PlatformType getType();

    void setType(@NotNull final PlatformType type);

    @NotNull
    String getVersion();

    void setVersion(@NotNull final String version);


}

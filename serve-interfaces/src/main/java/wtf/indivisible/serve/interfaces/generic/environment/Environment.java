package wtf.indivisible.serve.interfaces.generic.environment;

import com.sun.istack.internal.NotNull;

/**
 * Created 11/01/15.
 */
public interface Environment {

    @NotNull
    String getId();

    void setId(@NotNull final String id);

    @NotNull
    String getName();

    void setName(@NotNull final String name);

    @NotNull
    Platform getPlatform();

    void setPlatform(@NotNull final Platform platform);

}
